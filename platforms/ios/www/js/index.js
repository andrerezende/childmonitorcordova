/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

    // Wait for device API libraries to load
    //
    //////////////////////////////////////////////////////
    // Plugins to install
    // cordova plugin add org.apache.cordova.device
    // cordova plugin add org.apache.cordova.geolocation
    // https://build.phonegap.com/plugins/2056
    // cordova plugin add de.appplant.cordova.plugin.background-mode
    // cordova plugin add https://github.com/christocracy/cordova-plugin-background-geolocation.git
    // 
    // Olhar esse IOS Android e WP8
    //    org.transistorsoft.cordova.background-geolocation
    //     CDVBackgroundGeoLocation
    //     christocracy/cordova-plugin-background-geolocation <- maybe the best
    //
    //   Barcode scanner
    //   
    //   cordova plugin add https://github.com/wildabeast/BarcodeScanner.git
    //    cordova.plugins.barcodeScanner.scan(
    //  function (result) {
    //      alert("We got a barcode\n" +
    //            "Result: " + result.text + "\n" +
    //            "Format: " + result.format + "\n" +
    //            "Cancelled: " + result.cancelled);
    //  }, 
    //  function (error) {
    //      alert("Scanning failed: " + error);
    //  }
    // );
    //////////////////////////////////////////////////////
    // 
    // device APIs are available
    var watchID;
    function onDeviceReady() {
        // Now safe to use device APIs
        $("#idDevice").text(GetDeviceId());
        
        // Options: throw an error if no update is received every 30 seconds.
        watchID = navigator.geolocation.watchPosition(onSuccessGPS, onErrorGPS, { enableHighAccuracy: true, timeout: 5000, maximumAge: 0 });
    }
    ////////////////////////////////////////////////////////////////////////////
    function GetDeviceId() {
        // Now safe to use device APIs
        return(device.model+"_"+device.uuid+"_"+device.version);
    }

////////////////////////////////////////////////////////////////////////////////
// Main window
    
    document.addEventListener("deviceready", onDeviceReady, false); 
    
    PaintBackground();
     
    var container= 
        "<form id=\"formConfiguration\" action=\"\" > \
        <h3>Childmonitor Configuration</h3> \
        <a id='idDevice'>Device ID</a><br>\
        <a id='ServerStatus'>Server Status</a><br>\
        <a id=\"idRegDevice\" href=\"#\" class=\"s3d forrst\"> Device </a> <a id=\"idReadBarCode\" onclick=\"ReadBarcode()\" href=\"#\" class=\"s3d forrst\">Barcode</a><br> \
        <a >Email</a><br>\
        <a id='email'>email</a><br>\
        <a id='position'>position</a><br>\
        <a id='stLocationService'>Location Service Status</a><br>\
        <a id=\"idStartService\" href=\"#\" class=\"s3d forrst\">Start Service</a> <a id=\"idStopService\" href=\"#\" class=\"s3d forrst\"> Stop Service </a><br> \
        <br>\n\
        <br>\n\
        </form>";

    
    document.write(divRelative(container));



    // console.log('aasssssssssssssssssasdhbasdcfgasdkjfgasdkjfgaskjfgasdkjfgasdkjfgasdkjfgaskjdfgkajsdgfkjasdgfkjhasgfiuqgfiuasdgfkjhagsdkfjhgasdkfjgasdkjfgasdkjfgsadjkfgasdkjf');
    // 
////////////////////////////////////////////////////////////////////////////////    // 
// 
// onSuccess Callback
// This method accepts a Position object, which contains the
// current GPS coordinates
//
function ReadBarcode() 
{
    alert("opa");
    cordova.plugins.barcodeScanner.scan(
    function (result) {
      alert("We got a barcode\n" +
            "Result: " + result.text + "\n" +
            "Format: " + result.format + "\n" +
            "Cancelled: " + result.cancelled);
    }, 
    function (error) {
      alert("Scanning failed: " + error);
    }
    );    
    
}
function onSuccessGPS(position) 
{
    // .toLocaleFormat(formatString);
    var dateLoc = new Date(position.timestamp);
    // .toLocaleFormat('Y-m-d H:i:s')
    
    var buffer =  DateFormat(dateLoc) + '<br>' +
                  'Lat: '          + position.coords.latitude          + '<br>' +
                  'Lng: '         + position.coords.longitude         + '<br>' +
                  'Alt: '          + position.coords.altitude          + '<br>' +
                  'Acc: '          + position.coords.accuracy          + '<br>' +
                  'Heading: '           + position.coords.heading           + '<br>' +
                  'Speed: '             + position.coords.speed;
  
    $("#position").html(buffer);
    
    /*
    alert('Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' +
          'Altitude: '          + position.coords.altitude          + '\n' +
          'Accuracy: '          + position.coords.accuracy          + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
          'Heading: '           + position.coords.heading           + '\n' +
          'Speed: '             + position.coords.speed             + '\n' +
          'Timestamp: '         + position.timestamp                + '\n');
    */      
}

// onError Callback receives a PositionError object
//
function onErrorGPS(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}
////////////////////////////////////////////////////////////////////////////////
function DateFormat(date)
{
    var month=date.getMonth()+1;
    return(date.getFullYear()+"-"+month+"-"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds());
}
////////////////////////////////////////////////////////////////////////////////
function PaintBackground()
{
    document.write("<div style=\"background: rgba(255,255,255,1); \
                    background: -moz-linear-gradient(left, rgba(255,255,255,1) 0%, rgba(179,213,230,1) 100%); \
                    background: -webkit-gradient(left top, right top, color-stop(0%, rgba(255,255,255,1)), color-stop(100%, rgba(179,213,230,1))); \
                    background: -webkit-linear-gradient(left, rgba(255,255,255,1) 0%, rgba(179,213,230,1) 100%); \
                    background: -o-linear-gradient(left, rgba(255,255,255,1) 0%, rgba(179,213,230,1) 100%); \
                    background: -ms-linear-gradient(left, rgba(255,255,255,1) 0%, rgba(179,213,230,1) 100%); \
                    background: linear-gradient(to right, rgba(255,255,255,1) 0%, rgba(179,213,230,1) 100%); \
                    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#b3d5e6', GradientType=1 ); \
                    position:absolute; z-index:-50; \
                    top:0px; left:0px; width:100%; height:100%; opacity:0.9; border-radius: 0px; -moz-border-radius: 0px; \"  ></div>");
}
////////////////////////////////////////////////////////////////////////////////
function divRelative(container,opacity,top,left,width,height,borderradius)
{ 
    opacity = opacity || "1";
    top=top||"0px";
    left=left||"0px";
    width=width||"100%";
    height=height||"100%";
    borderradius=borderradius||"0px";
 
    var buffer = "<div style=\" background: linear-gradient(to bottom, rgba(174,174,174,1) 0%,rgba(22,5,22,1) 100%);   box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -moz-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); -webkit-box-shadow: 1px 2px 6px rgba(0, 0, 0, 0.5); position:absolute; z-index:1; \
                  top:"+top+"; left:"+left+"; width:"+width+"; height:"+height+"; opacity:"+opacity+"; border-radius: "+borderradius+"; -moz-border-radius: 6px; \"  >";
    buffer = buffer + container;
    buffer = buffer + "</div>";       
    return buffer;
}
// estilos de div
// background-color:Black; z-index: 0; opacity:0.5; position:absolute;
// 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


